<?php

namespace Fstar\Coordinate\Svc;

use GuzzleHttp\Client;

class CoordinateService {
    private $pi;
    private $x_pi;
    private $a;
    private $ee;
    private $bd_lon_gap;
    private $bd_lat_gap;
    private $bd_ak;

    public function __construct($config) {
        $this->pi = data_get($config, 'pi');
        $this->x_pi = data_get($config, 'x_pi');
        $this->a = data_get($config, 'a');
        $this->ee = data_get($config, 'ee');
        $this->bd_lon_gap = data_get($config, 'bd_lon_gap');
        $this->bd_lat_gap = data_get($config, 'bd_lat_gap');
        $this->bd_ak = data_get($config, 'bd_ak');
    }

    public function newInstanse() {
        return $this;
    }

    /**
     * BD-09 转 GCJ-02
     *
     * @param $bd_lat
     * @param $bd_lon
     *
     * @return float[]
     */
    public function bd09ToGCJ02($bd_lat, $bd_lon) {
        $x = $bd_lon - $this->bd_lon_gap;
        $y = $bd_lat - $this->bd_lat_gap;
        $z = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $this->x_pi);
        $theta = atan2($y, $x) - 0.000003 * cos($x * $this->x_pi);
        $tempLon = $z * cos($theta);
        $tempLat = $z * sin($theta);
        return ['lat' => $tempLat, 'lon' => $tempLon];
    }

    /**
     * GCJ-02 转 BD-09
     *
     * @param $lat
     * @param $lon
     *
     * @return float[]
     */
    public function gcj02ToBD09($lat, $lon) {
        $x = $lon;
        $y = $lat;
        $z = sqrt($x * $x + $y * $y) + 0.00002 * sin($y * $this->x_pi);
        $theta = atan2($y, $x) + 0.000003 * cos($x * $this->x_pi);
        $tempLon = $z * cos($theta) + $this->bd_lon_gap;
        $tempLat = $z * sin($theta) + $this->bd_lat_gap;
        return ['lat' => $tempLat, 'lon' => $tempLon];
    }

    public function wgs84ToGCJ02($lat, $lon) {
        if($this->outOfChina($lat, $lon)) {
            return ['lat' => $lat, 'lon' => $lon];
        }
        return $this->transform($lat, $lon);
    }

    public function gcj02ToWGS84($lat, $lon) {
        if($this->outOfChina($lat, $lon)) {
            return ['lat' => $lat, 'lon' => $lon];
        }
        $gps = $this->transform($lat, $lon);
        return ['lat' => $lat * 2 - $gps['lat'], 'lon' => $lon * 2 - $gps['lon']];
    }

    public function wgs84ToBD09($lat, $lon) {
        $gcj02 = $this->wgs84ToGCJ02($lat, $lon);
        return $this->gcj02ToBD09($gcj02['lat'], $gcj02['lon']);
    }

    public function bd09ToWGS84($bd_lat, $bd_lon) {
        $gcj02 = $this->bd09ToGCJ02($bd_lat, $bd_lon);
        return $this->gcj02ToWGS84($gcj02['lat'], $gcj02['lon']);
    }

    public function bd09ToGCJ02Batch($coords_str) {
        $coords = $this->coordsStrToArry($coords_str);
        foreach($coords as $idx => $coord) {
            $coords[$idx] = $this->bd09ToGCJ02($coord['lat'], $coord['lon']);
        }
        return $coords;
    }

    public function bd09ToWGS84Batch($coords_str) {
        $coords = $this->coordsStrToArry($coords_str);
        foreach($coords as $idx => $coord) {
            $coords[$idx] = $this->bd09ToWGS84($coord['lat'], $coord['lon']);
        }
        return $coords;
    }

    public function gcj02ToBD09Batch($coords_str) {
        $coords = $this->coordsStrToArry($coords_str);
        foreach($coords as $idx => $coord) {
            $coords[$idx] = $this->gcj02ToBD09($coord['lat'], $coord['lon']);
        }
        return $coords;
    }

    public function gcj02ToWGS84Batch($coords_str) {
        $coords = $this->coordsStrToArry($coords_str);
        foreach($coords as $idx => $coord) {
            $coords[$idx] = $this->gcj02ToWGS84($coord['lat'], $coord['lon']);
        }
        return $coords;
    }

    public function wgs84ToBD09Batch($coords_str) {
        $coords = $this->coordsStrToArry($coords_str);
        foreach($coords as $idx => $coord) {
            $coords[$idx] = $this->wgs84ToBD09($coord['lat'], $coord['lon']);
        }
        return $coords;
    }

    public function wgs84ToGCJ02Batch($coords_str) {
        $coords = $this->coordsStrToArry($coords_str);
        foreach($coords as $idx => $coord) {
            $coords[$idx] = $this->wgs84ToGCJ02($coord['lat'], $coord['lon']);
        }
        return $coords;
    }

    /**
     * @param $coords  经度,纬度;经度,纬度;经度,纬度;经度,纬度  最多100组
     *
     * @return array
     * @throws CoordinateException
     */
    public function gcj02ToBD09Exact($coords_str) {
        return $this->doBdGeoConv($coords_str);
    }

    public function wgs84ToBD09Exact($coords_str) {
        return $this->doBdGeoConv($coords_str, 1);
    }

    public function bd09ToGCJ02Exact($coords_str) {
        return $this->doBdGeoConv($coords_str, 5, 3);
    }

    public function coordsStrToArry($coord_str) {
        $ret = [];
        $coords = explode(';', $coord_str);
        foreach($coords as $coord) {
            $coord_arry = explode(',', $coord);
            if(count($coord_arry) != 2) {
                throw  new CoordinateException("坐标参数错误");
            }
            $ret[] = ['lat' => $coord_arry[1], 'lon' => $coord_arry[0]];
        }
        return $ret;
    }

    private function doBdGeoConv($coords_str, $from = 3, $to = 5) {
        $max_len = 100;
        $coords_arry = explode(';', $coords_str);
        $len = count($coords_arry);
        $times = ceil($len / $max_len);
        $ret_data = [];
        for($idx = 0; $idx < $times; $idx ++) {
            $coords = $this->bdGeoConv(implode(';', array_slice($coords_arry, $idx * $max_len, $max_len)), $from, $to);
            $ret_data = array_merge($ret_data, $coords);
        }
        return $ret_data;
    }

    private function bdGeoConv($coords_str, $from = 3, $to = 5) {
        $url = "https://api.map.baidu.com/geoconv/v1/";
        $params = [
            'coords' => $coords_str,
            'from'   => $from,
            'to'     => $to,
            'ak'     => $this->bd_ak
        ];
        try {
            $response = (new Client())->request('GET', $url, [
                'timeout'         => 60,
                'connect_timeout' => 5,
                'verify'          => false,
                'query'           => $params
            ]);
            $content = $response->getBody()->getContents();
            if(!is_array($content)) {
                $content = json_decode($content, true);
            }
            if($content['status'] != 0) {
                throw  new CoordinateException("坐标转换错误,错误码:{$content['status']}");
            }
            $coords = [];
            foreach($content['result'] as $coord) {
                $coords[] = ['lat' => $coord['y'], 'lon' => $coord['x']];
            }
            return $coords;
        } catch(\Exception $ex) {
            throw new CoordinateException($ex->getMessage());
        }
    }

    private function transformLat($x, $y) {
        $ret = - 100.0 + 2.0 * $x + 3.0 * $y + 0.2 * $y * $y + 0.1 * $x * $y + 0.2 * sqrt(abs($x));
        $ret += (20.0 * sin(6.0 * $x * $this->pi) + 20.0 * sin(2.0 * $x * $this->pi)) * 2.0 / 3.0;
        $ret += (20.0 * sin($y * $this->pi) + 40.0 * sin($y / 3.0 * $this->pi)) * 2.0 / 3.0;
        $ret += (160.0 * sin($y / 12.0 * $this->pi) + 320 * sin($y * $this->pi / 30.0)) * 2.0 / 3.0;
        return $ret;
    }

    private function transformLon($x, $y) {
        $ret = 300.0 + $x + 2.0 * $y + 0.1 * $x * $x + 0.1 * $x * $y + 0.1 * sqrt(abs($x));
        $ret += (20.0 * sin(6.0 * $x * $this->pi) + 20.0 * sin(2.0 * $x * $this->pi)) * 2.0 / 3.0;
        $ret += (20.0 * sin($x * $this->pi) + 40.0 * sin($x / 3.0 * $this->pi)) * 2.0 / 3.0;
        $ret += (150.0 * sin($x / 12.0 * $this->pi) + 300.0 * sin($x / 30.0 * $this->pi)) * 2.0 / 3.0;
        return $ret;
    }

    private function transform($lat, $lon) {
        $dLat = $this->transformLat($lon - 105.0, $lat - 35.0);
        $dLon = $this->transformLon($lon - 105.0, $lat - 35.0);
        $radLat = $lat / 180.0 * $this->pi;
        $magic = sin($radLat);
        $magic = 1 - $this->ee * $magic * $magic;
        $sqrtMagic = sqrt($magic);
        $dLat = ($dLat * 180.0) / (($this->a * (1 - $this->ee)) / ($magic * $sqrtMagic) * $this->pi);
        $dLon = ($dLon * 180.0) / ($this->a / $sqrtMagic * cos($radLat) * $this->pi);
        $mgLat = $lat + $dLat;
        $mgLon = $lon + $dLon;
        return ['lat' => $mgLat, 'lon' => $mgLon];
    }

    private function outOfChina($lat, $lon) {
        return ($lon < 72.004 || $lon > 137.8347 || $lat < 0.8293 || $lat > 55.8271);
    }
}