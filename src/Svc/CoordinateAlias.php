<?php

namespace Fstar\Coordinate\Svc;

use Fstar\Coordinate\Constants;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Fstar\Coordinate\Svc\CoordinateService
 *
 * string $coords_str $lon,$lat;$lon,$lat;$lon,$lat;$lon,$lat
 *
 * @method static CoordinateService newInstanse()
 * @method static bd09ToGCJ02(float $bd_lat, float $bd_lon)
 * @method static bd09ToWGS84(float $bd_lat, float $bd_lon)
 * @method static gcj02ToBD09(float $lat, float $lon)
 * @method static gcj02ToWGS84(float $lat, float $lon)
 * @method static wgs84ToGCJ02(float $lat, float $lon)
 * @method static wgs84ToBD09(float $lat, float $lon)
 * @method static bd09ToGCJ02Batch(string $coords_str)
 * @method static bd09ToWGS84Batch(string $coords_str)
 * @method static gcj02ToBD09Batch(string $coords_str)
 * @method static gcj02ToWGS84Batch(string $coords_str)
 * @method static wgs84ToBD09Batch(string $coords_str)
 * @method static wgs84ToGCJ02Batch(string $coords_str)
 * @method static gcj02ToBD09Exact(string $coords_str)
 * @method static wgs84ToBD09Exact(string $coords_str)
 * @method static bd09ToGCJ02Exact(string $coords_str)
 */
class CoordinateAlias extends Facade {
    protected static function getFacadeAccessor() {
        return Constants::lib_coordinate;
    }
}