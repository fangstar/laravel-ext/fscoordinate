# fscoordinate

#### Description

fangstar coordinate transform tool

#### Software Architecture

laravel

#### Installation

``` php
1. composer require fstar/coordinate
2. php artisan vendor:publish --provider=Fstar\Coordinate\CoordinateServiceProvider

```

#### Instructions

``` php
$coords_str string  $lon,$lat;$lon,$lat;$lon,$lat;$lon,$lat
FsCoordinate::newInstanse()
FsCoordinate::bd09ToGCJ02(float $bd_lat, float $bd_lon)
FsCoordinate::bd09ToWGS84(float $bd_lat, float $bd_lon)
FsCoordinate::gcj02ToBD09(float $lat, float $lon)
FsCoordinate::gcj02ToWGS84(float $lat, float $lon)
FsCoordinate::wgs84ToGCJ02(float $lat, float $lon)
FsCoordinate::wgs84ToBD09(float $lat, float $lon)
FsCoordinate::bd09ToGCJ02Batch(string $coords_str)
FsCoordinate::bd09ToWGS84Batch(string $coords_str)
FsCoordinate::gcj02ToBD09Batch(string $coords_str)
FsCoordinate::gcj02ToWGS84Batch(string $coords_str)
FsCoordinate::wgs84ToGCJ02Batch(string $coords_str)
FsCoordinate::wgs84ToBD09Batch(string $coords_str)
FsCoordinate::gcj02ToBD09Exact(string $coords_str)
FsCoordinate::wgs84ToBD09Exact(string $coords_str)
```

#### Contribution

1. Fangstar