<?php

namespace Fstar\Coordinate;

use Fstar\Coordinate\Svc\CoordinateService;
use Illuminate\Support\ServiceProvider;

class CoordinateServiceProvider extends ServiceProvider {
    public function boot() {
        // Publish the configuration file
        $this->publishes([__DIR__."/config/fstar-coordinate.php" => config_path(Constants::conf_name.".php")]);
    }

    public function register() {
        $this->mergeConfigFrom(__DIR__."/config/fstar-coordinate.php", Constants::conf_name);
        $user_config = $this->app['config'][Constants::conf_name];
        $this->app->singleton(Constants::lib_coordinate, function($app) use ($user_config) {
            return new CoordinateService($user_config);
        });
    }

    public function provides() {
        return [
            Constants::lib_coordinate
        ];
    }
}